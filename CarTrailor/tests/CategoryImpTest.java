import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import interfaces.Category;

class CategoryImpTest extends SetUp {
	private Category catTest1;
	
	@BeforeEach
	public void setUp() {
		super.setUp();
		catTest1 = new CategoryImp("Engine");
	}

	@Test
	void testGetName() {
		assertEquals(catEngine.getName(), "Engine");
		assertEquals(catTransmission.getName(), "Transmission");
		assertEquals(catExterior.getName(), "Exterior");
		assertEquals(catInterior.getName(), "Interior");
	}
	
	@Test
	void testEquals() {
		assertTrue(catEngine.equals(catTest1));
		assertFalse(catTransmission.equals(catTest1));
		assertFalse(catExterior.equals(catTest1));
		assertFalse(catInterior.equals(catTest1));
		assertFalse(catEngine.equals(null));
	}
}
