import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import interfaces.PartType;
import rules.ConflictingRuleException;

class CompatibilityManagerImpTest extends SetUp {
	@BeforeEach
	public void setUp() {
		super.setUp();
		
	}
	
	@Test
	void testGetIncompatibilities() {
		assertTrue(cm.getIncompatibilities(pTSF7).get().containsAll(sIncompTSF7) && sIncompTSF7.containsAll(cm.getIncompatibilities(pTSF7).get()));
		assertTrue(cm.getIncompatibilities(pIS).get().containsAll(sIncompIS) && sIncompIS.containsAll(cm.getIncompatibilities(pIS).get()));
		assertTrue(cm.getIncompatibilities(pXC).get().containsAll(sIncompXC) && sIncompXC.containsAll(cm.getIncompatibilities(pXC).get()));
	}
	
	@Test
	void testGetRequirements() {
		assertTrue(cm.getRequirements(pEH120).get().containsAll(sRequirEH120) && sRequirEH120.containsAll(cm.getRequirements(pEH120).get()));
		assertTrue(cm.getRequirements(pTC120).get().containsAll(sRequirTC120) && sRequirTC120.containsAll(cm.getRequirements(pTC120).get()));
		assertTrue(cm.getRequirements(pXS).get().containsAll(sRequirXS) && sRequirXS.containsAll(cm.getRequirements(pXS).get()));
		assertTrue(cm.getRequirements(pIS).get().containsAll(sRequirIS) && sRequirIS.containsAll(cm.getRequirements(pIS).get()));
	}
	
	@Rule
    public ExpectedException exception = ExpectedException.none();
	
	@Test
	void testAddIncompatibilities2() throws ConflictingRuleException {
		exception.expect(ConflictingRuleException.class);
		cm.addIncompatibilities(pXS, new HashSet<PartType>(Arrays.asList(pIS)));
	}
}
