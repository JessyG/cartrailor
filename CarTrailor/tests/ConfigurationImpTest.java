import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import interfaces.Category;
import interfaces.Configuration;
import interfaces.Configurator;
import interfaces.PartType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConfigurationImpTest extends SetUp {
	private Configuration ci;
	private Configurator configurator;
	private Set<PartType> severalPartTypes;
	private Set<Category> severalCategories;
	
	@BeforeEach
	public void setUp() {
		super.setUp();
		severalPartTypes = new HashSet<PartType>();
		severalPartTypes.addAll(sIncompTSF7);
		severalPartTypes.addAll(sRequirEH120);
		severalCategories = new HashSet<Category>();
		severalCategories.add(catEngine);
		severalCategories.add(catTransmission);
		severalCategories.add(catExterior);
		severalCategories.add(catInterior);
		configurator = new ConfiguratorImp(ci, severalCategories, severalPartTypes, cm);
	}
	
	@Test
	void testValidTrue() {
		Set<PartType> selectPartTypes = new HashSet<PartType>(Arrays.asList(pTS6, pEG100));
		ci = new ConfigurationImp(selectPartTypes);
		ci.setConfigurator(configurator);
		assertTrue(ci.isValid());
	}
	
	@Test
	void testValidFalse() {
		Set<PartType> selectPartTypes = new HashSet<PartType>(Arrays.asList(pTA5, pEG100));
		ci = new ConfigurationImp(selectPartTypes);
		ci.setConfigurator(configurator);
		assertFalse(ci.isValid());
	}
	
	@Test
	void testCompleteFalse() {
		Set<PartType> selectPartTypes = new HashSet<PartType>(Arrays.asList(pTA5, pEG100));
		ci = new ConfigurationImp(selectPartTypes);
		ci.setConfigurator(configurator);
		assertFalse(ci.isComplete());
	}
	
	@Test
	void testCompleteTrue() {
		Set<PartType> selectPartTypes = new HashSet<PartType>(Arrays.asList(pEG133, pTM5, pXM, pIH));
		ci = new ConfigurationImp(selectPartTypes);
		ci.setConfigurator(configurator);
		assertTrue(ci.isComplete());
	}
	
	@Test
	void testSelectPartTrue() {
		ci = new ConfigurationImp(new HashSet<PartType>());
		ci.setConfigurator(configurator);
		assertTrue(ci.selectPart(pEG133));
		assertEquals(ci.getSelectedParts(), new HashSet<PartType>(Arrays.asList(pEG133)));
	}
	
	@Test
	void testSelectPartFalse() {
		ci = new ConfigurationImp(new HashSet<PartType>(Arrays.asList(pIS)));
		ci.setConfigurator(configurator);
		assertFalse(ci.selectPart(pEG100));
		assertEquals(ci.getSelectedParts(), new HashSet<PartType>(Arrays.asList(pIS)));
	}
	
	@Test
	void testUnselectPartType() {
		ci = new ConfigurationImp(new HashSet<PartType>(Arrays.asList(pIS)));
		ci.setConfigurator(configurator);
		ci.unselectPartType(catInterior);
		assertEquals(ci.getSelectedParts(), new HashSet<PartType>());
	}
	
	@Test
	void testClear() {
		ci = new ConfigurationImp(new HashSet<PartType>(Arrays.asList(pIS, pEG100, pTC120)));
		ci.setConfigurator(configurator);
		ci.clear();
		assertEquals(ci.getSelectedParts(), new HashSet<PartType>());
	}
}
