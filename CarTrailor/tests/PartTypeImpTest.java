import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import interfaces.PartType;

class PartTypeImpTest extends SetUp {
	private PartType ptest1;
	private PartType ptest2;
	private PartType ptest3;
	
	@BeforeEach
	public void setUp() {
		super.setUp();
		ptest1 = new PartTypeImp("EG100", catEngine);
		ptest2 = new PartTypeImp("E100", catEngine);
		ptest2 = new PartTypeImp("E100", catTransmission);
	}
	
	@Test
	void testGetName() {
		assertEquals(pEG100.getName(), "EG100");
		assertEquals(pTM5.getName(), "TM5");
		assertEquals(pXC.getName(), "XC");
	}
	
	@Test
	void testGetCategory() {
		assertEquals(pEG100.getCategory(), catEngine);
		assertEquals(pTM5.getCategory(), catTransmission);
		assertEquals(pXM.getCategory(), catExterior);
		assertEquals(pIS.getCategory(), catInterior);
	}
	
	@Test
	void testEquals() {
		assertTrue(pEG100.equals(ptest1));
		assertFalse(pEG100.equals(ptest2));
		assertFalse(pEG100.equals(ptest3));
	}
}
