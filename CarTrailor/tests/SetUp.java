import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;

import interfaces.Category;
import interfaces.CompatibilityManager;
import interfaces.PartType;

class SetUp {
		public Category catEngine;
		public Category catTransmission;
		public Category catExterior;
		public Category catInterior;
		
		public PartType pEG100;
		public PartType pEG133;
		public PartType pEG210;
		public PartType pED110;
		public PartType pED180;
		public PartType pEH120;
		
		public PartType pTM5;
		public PartType pTM6;
		public PartType pTA5;
		public PartType pTS6;
		public PartType pTSF7;
		public PartType pTC120;
		
		public PartType pXC;
		public PartType pXM;
		public PartType pXS;
		
		public PartType pIN;
		public PartType pIH;
		public PartType pIS;

		public Map<PartType, Set<PartType>> incompatibilities;
		public Map<PartType, Set<PartType>> requirements;
		public Set<PartType> spEG100;
		public Set<PartType> sIncompIS;
		public Set<PartType> sIncompTSF7;
		public Set<PartType> sIncompXC;
		public Set<PartType> sRequirEH120;
		public Set<PartType> sRequirTC120;
		public Set<PartType> sRequirXS;
		public Set<PartType> sRequirIS;
		public Set<PartType> allPartTypes;
		
		public CompatibilityManager cm;
		
		@BeforeEach
		public void setUp() {
			catEngine = new CategoryImp("Engine");
			catTransmission = new CategoryImp("Transmission");
			catExterior = new CategoryImp("Exterior");
			catInterior = new CategoryImp("Interior");
			
			pEG100 = new PartTypeImp("EG100", catEngine);
			pEG133 = new PartTypeImp("EG133", catEngine);
			pEG210 = new PartTypeImp("EG210", catEngine);
			pED110 = new PartTypeImp("ED110", catEngine);
			pED180 = new PartTypeImp("ED180", catEngine);
			pEH120 = new PartTypeImp("EH120", catEngine);
			
			pTM5 = new PartTypeImp("TM5", catTransmission);
			pTM6 = new PartTypeImp("TM6", catTransmission);
			pTA5 = new PartTypeImp("TA5", catTransmission);
			pTS6 = new PartTypeImp("TS6", catTransmission);
			pTSF7 = new PartTypeImp("TSF7", catTransmission);
			pTC120 = new PartTypeImp("TC120", catTransmission);
			
			pXC = new PartTypeImp("XC", catExterior);
			pXM = new PartTypeImp("XM", catExterior);
			pXS = new PartTypeImp("XS", catExterior);
			
			pIN = new PartTypeImp("IN", catInterior);
			pIH = new PartTypeImp("IH", catInterior);
			pIS = new PartTypeImp("IS", catInterior);
			
			
			sIncompTSF7 = new HashSet<PartType>(Arrays.asList(pEG100, pEG133, pED110));
			spEG100 = new HashSet<PartType>(Arrays.asList(pEG100));
			sIncompXC = new HashSet<PartType>(Arrays.asList(pEG210));
			sIncompIS = new HashSet<PartType>(Arrays.asList(pEG100, pTM5));
			
			sRequirEH120 = new HashSet<PartType>(Arrays.asList(pTC120));
			sRequirTC120 = new HashSet<PartType>(Arrays.asList(pEH120));
			sRequirXS = new HashSet<PartType>(Arrays.asList(pIS));
			sRequirIS = new HashSet<PartType>(Arrays.asList(pXS));
			
			incompatibilities = new HashMap<PartType, Set<PartType>>();
			incompatibilities.put(pTA5, spEG100);
			incompatibilities.put(pTSF7, sIncompTSF7);
			incompatibilities.put(pXC, sIncompXC);
			incompatibilities.put(pXM, spEG100);
			incompatibilities.put(pXS, spEG100);
			incompatibilities.put(pIS, sIncompIS);
			
			requirements = new HashMap<PartType, Set<PartType>>();
			requirements.put(pEH120, sRequirEH120);
			requirements.put(pTC120, sRequirTC120);
			requirements.put(pXS, sRequirXS);
			requirements.put(pIS, sRequirIS);
			
			cm = new CompatibilityManagerImp(incompatibilities, requirements);
		}

}
