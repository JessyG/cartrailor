import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import interfaces.Category;
import interfaces.Configuration;
import interfaces.Configurator;
import interfaces.PartType;

class ConfiguratorImpTest extends SetUp {
	private Configuration ci;
	private Configurator configurator;
	private Set<PartType> severalPartTypes;
	private Set<Category> severalCategories;
	
	@BeforeEach
	public void setUp() {
		super.setUp();
		severalPartTypes = new HashSet<PartType>();
		severalPartTypes.addAll(sIncompTSF7);
		severalPartTypes.addAll(sRequirEH120);
		severalCategories = new HashSet<Category>();
		severalCategories.add(catEngine);
		severalCategories.add(catTransmission);
		severalCategories.add(catExterior);
		severalCategories.add(catInterior);
		ci = new ConfigurationImp(new HashSet<PartType>());
		configurator = new ConfiguratorImp(ci, severalCategories, severalPartTypes, cm);
	}
	
	@Test
	void testGetCategories() {
		assertEquals(severalCategories, configurator.getCategories());
	}
	
	@Test
	void testGetVariants() {
		assertEquals(new HashSet<PartType>(Arrays.asList(pIN, pIH, pIS)), configurator.getVariants(catInterior));
	}
	
	@Test
	void testGetConfiguration() {
		assertEquals(ci, configurator.getConfiguration());
	}

	@Test
	void testGetCompatibilityChecker() {
		assertEquals(cm, configurator.getCompatibilityChecker());
	}
}
