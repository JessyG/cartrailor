import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import interfaces.Category;
import interfaces.CompatibilityChecker;
import interfaces.CompatibilityManager;
import interfaces.Configuration;
import interfaces.Configurator;
import interfaces.PartType;

/**
 * <b>Implementation class of Configuration.</b>
 * 
 * @see Configuration
 * 
 * @author Jessy Grall
 * @author Kassim Kaba
 */
public class ConfigurationImp implements Configuration, Cloneable {
	/**
	 * access point data
	 * 
	 * @see Configurator
	 */
	private static Configurator configurator;
	/**
	 * set of PartType selected by the user
	 * <p>
	 * It is possible to add or remove a PartType of the set
	 * </p>
	 * 
	 * @see PartType
	 */
	private Set<PartType> selectedParts;
	
	/**
	 * class constructor
	 * 
	 * <p>
	 * When building an object ConfigurationImp, selectedParts is initialized to empty set,
	 * </p>
	 * 
	 * @see ConfigurationImp#selectedParts
	 */
	ConfigurationImp(Set<PartType> sp) {
		selectedParts = sp;
	}
	
	public void setConfigurator(Configurator conf) {
		configurator = conf;
	}
	
	@Override
	public boolean isValid() {
		CompatibilityManager checker = configurator.getCompatibilityChecker();
		Set<PartType> requires;
		Optional<Set<PartType>> requiresOp = Optional.empty();
		Optional<Set<PartType>> incompatibilitiesOp = Optional.empty();
		PartType ref = null;
		for(Iterator<PartType> it1 = selectedParts.iterator(); it1.hasNext();) {
			ref = it1.next();
			requiresOp = checker.getRequirements(ref);
			if(requiresOp.isPresent()) {
				requires = requiresOp.get();
				// Gestion des requirements
				for(PartType p : requires)
					if(!selectedParts.contains(p)) return false;
			}
			// Gestion des incompatibilités
			for(Iterator<PartType> it2 = it1; it2.hasNext();) {
				incompatibilitiesOp = checker.getIncompatibilities(it2.next());
				if(incompatibilitiesOp.isPresent())
					if(incompatibilitiesOp.get().contains(ref)) return false;
			}
		}
		return true;
	}

	@Override
	public boolean isComplete() {
		Set<Category> cat = configurator.getCategories();
		char result = 0;
		for(Category c : cat) {
			for(PartType p : selectedParts) {
				if(p.getCategory().equals(c)) {
					result++;
					break;
				}
			}
			if(result != 1) return false;
			result = 0;
		}
		return true;
	}
	
	@Override
	public Set<PartType> getSelectedParts() {
		return new HashSet<>(selectedParts);
	}
	
	@Override
	public boolean selectPart(PartType chosenPart) {
		Objects.requireNonNull(chosenPart);
		selectedParts.add(chosenPart);
		if(!isValid()) {
			selectedParts.remove(chosenPart);
			return false;
		}
		return true;
	}

	@Override
	public Optional<PartType> getSelectionForCategory(Category category) {
		Objects.requireNonNull(category);
		for(PartType p : selectedParts)
			if(p.getCategory().equals(category)) return Optional.of(p);
		return Optional.empty();
	}

	@Override
	public void unselectPartType(Category categoryToClear) {
		Objects.requireNonNull(categoryToClear);
		for(PartType p : selectedParts)
			if(p.getCategory().equals(categoryToClear)) selectedParts.remove(p);
	}

	@Override
	public void clear() {
		selectedParts.clear();
	}

	@Override
	public ConfigurationImp clone() {
		ConfigurationImp c = null;
		try {c = (ConfigurationImp) super.clone();}
		catch(CloneNotSupportedException e) {}
		return c;
	}
}
