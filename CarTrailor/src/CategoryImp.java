import interfaces.Category;

/**
 * Implementation class of Category
 * 
 * @see Category
 * 
 * @author Jessy Grall
 * @author Kassim Kaba
 */
public class CategoryImp implements Category {
	/**
	 * @Name
	 */
	private String name;//the categorie name
	
	/**
	 * the constructor of categorie 
	 * @param name , not null
	 * Name of the Category
	 */
	
	/**
	 * Class constructor
	 * 
	 * * <p>
	 * When building an object ConfigurationImp, we define the name of the Category.
	 * </p>
	 */
	public CategoryImp(String name) {
		this.name = name;
	}
	/**
	 * 
	 */
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(!(obj instanceof CategoryImp)) return false;
		CategoryImp c = (CategoryImp) obj;
		return c.name != null && this.name == c.name;
	}
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
}
