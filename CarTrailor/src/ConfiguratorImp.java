import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import interfaces.Category;
import interfaces.CompatibilityChecker;
import interfaces.CompatibilityManager;
import interfaces.Configuration;
import interfaces.Configurator;
import interfaces.PartType;

/**
 * Implementation class of Configurator
 * 
 * @see Configurator
 * 
 * @author Jessy Grall
 * @author Kassim Kaba
 */
public class ConfiguratorImp implements Configurator {
	/**
	 * Data container
	 * 
	 * @see Configuration
	 */
	private Configuration config;
	/**
	 * Set of Categories
	 * 
	 * @see Category
	 */
	private final Set<Category> categories;
	/**
	 * Set of PartTypes
	 * 
	 * @see PartType
	 */
	private final Set<PartType> partTypes;
	/**
	 * Compatibility rules manager
	 * 
	 * @see CompatibilityChecker
	 */
	private final CompatibilityManager compatibilityManager;
	
	
	/**
	 * Class constructor
	 * 
	 * <p>
	 * When building an object ConfiguratorImp, we define all categories, PartType and compatibility rules.
	 * </p>
	 */
	public ConfiguratorImp(Configuration config, Set<Category> c, Set<PartType> p, CompatibilityManager cm) {
		this.config = config;
		categories = c;
		partTypes = p;
		compatibilityManager = cm;
	}
	
	@Override
	public Set<Category> getCategories() {
		//return new HashSet<Category>(categories);
		return categories;
	}

	@Override
	public Set<PartType> getVariants(Category category) {
		Objects.requireNonNull(category);
		Set<PartType> partResult = new HashSet<PartType>();
		for(PartType p : partTypes)
			if(p.getCategory().getName().equals(category.getName()))
				partResult.add(p);
		return partResult;
	}

	@Override
	public Configuration getConfiguration() {
		return config.clone();
	}

	@Override
	public CompatibilityManager getCompatibilityChecker() {
		return compatibilityManager;
	}
}
