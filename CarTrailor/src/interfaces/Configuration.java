package interfaces;
import java.util.Optional;
/**
 * <b>Interface representing the data container.</b>
 * <p>
 * A configuration is represented by the following informations :
 * <ul>
 * <li>A Configurator to access data</li>
 * <li>A set of PartType</li>
 * </ul>
 * </p>
 * 
 * @see PartType
 * 
 * @author Jessy Grall
 * @author Kassim Kaba
 */
import java.util.Set;

public interface Configuration extends Cloneable {

	/**
	 * <p> 
	 * Check that :
	 * <ul>
	 * <li>There are not incompatibilities each PartType of the set.</li>
	 * <li>all the required parts of each part are also found in the set.</li>
	 * </ul>
	 * </p>
	 */
    boolean isValid();

    /**
	 * Check that we have only one PartType per Category
	 */
    boolean isComplete();

    /**
     * Return the selected PartTypes.
     * 
     * @return An instance of selectedParts.
     */
    Set<PartType> getSelectedParts();

    /**
	 * If possible add a PartType to the set
	 * 
	 * @param chosenPart
	 * 		The PartType to add to the set
	 * 
	 * @return 	true
	 * 		if the addition to the set has been made and the set remains valid
	 * 			false
	 * 		If the addition to the set has not been done because the configuration is found to be invalid
	 * 
	 * @see ConfigurationImp#isValid()
	 */
    boolean selectPart(PartType chosenPart);

    /**
     * Returns the PartType of the selectedParts set belonging to the specified Category
	 * 
	 * @param category
	 * 		Category of which we want to retrieve the corresponding PartType
	 * 
	 * @return A PartType belonging to the specified category if it exists, and null otherwise
	 * 
	 * @see Category
	 * @see PartType#getCategory()
	 */
    Optional<PartType> getSelectionForCategory(Category category);

    /**
	 * Removes the PartType of the specified Category from the selectedParts set.
	 * 
	 * @param categoryToClear
	 * 
	 */
    void unselectPartType(Category categoryToClear);

    /**
	 * Clear selectedParts
	 */
    void clear();

    /**
	 * Returns an instance of the ConfigurationImp class
	 * 
	 * @return An instance of the class
	 * 
	 */
    public Configuration clone();

	void setConfigurator(Configurator configurator);
}