package interfaces;
import java.util.Optional;
import java.util.Set;
/**
 * 
 * @author jessy
 *
 */
public interface CompatibilityChecker {
/**
 *  Returns the immutable set of PartType  values for given the PartType reference .
	 * For a PartType reference, it returns all Incompatibilities
	 * or for a non existing PartType reference , returns an empty set.
 * @param reference , is the partType reference and not-null
 * @return an immutable set of PartType 
 */
    Optional<Set<PartType>> getIncompatibilities(PartType reference);
    /**
     *  Returns the immutable set of PartType  values for given the PartType reference .
    	 * For a PartType reference, it returns all requirements
    	 * or for a non existing PartType reference , returns an empty set.
     * @param reference , is the partType reference and not-null
     * @return an immutable set of PartType 
     */
    Optional<Set<PartType>> getRequirements(PartType reference);

}