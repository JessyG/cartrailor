package interfaces;
import java.util.Set;

import rules.ConflictingRuleException;

public interface CompatibilityManager extends CompatibilityChecker {

	/**
	 * <p>
	 * 		For a PartType reference, add a set of its incompatibilities in the configuration
	 * 		it checks if for all containers of the target if the references wasn't  require
	 * 
	 *	<ul>
	 * 		<li>it adds to incompatibilities in the configuration if the PartType isn't existed in the configuration
	 *  		or adds the target to the set of reference if it exists
	 *  	</li>
		 * </ul>
		 * </p>
	 * @param reference , the PartType, it is not null
	 * @param target, a set of PartType, it must not be null
	 * @throws ConflictingRuleException returns an error if its has an element of target in reference requirements
	 * @see PartType 
	 * @see Configuration 
	 */
    void addIncompatibilities(PartType reference, Set<PartType> target) throws ConflictingRuleException;

    /**
	 * <p>
	 * 		For a PartType reference, add a PartType target  its incompatibilities in the configuration
	 * 		
	 *	<ul>
	 * 		<li>
	 * 			it removes the target to incompatibilities in the configuration if the PartType reference exists in the configuration
	 *  	</li>
		 * </ul>
		 * </p>
	 * @param reference , the PartType, it is not null
	 * @param target, a PartType, it must not be null
	 * @see PartType 
	 * @see Configuration 
	 */
    void removeIncompatibility(PartType reference, PartType target);

    /**
	 * <p>
	 * 		For a PartType reference, add a set of its requirements in the configuration
	 * 		it checks if for all containers of the target if the references wasn't  an reference's incompatibility
	 * 
	 *	<ul>
	 * 		<li>it adds to requirements in the configuration if the PartType isn't existed in the configuration
	 *  		or adds the target to the set of reference if it exists
	 *  	</li>
		 * </ul>
		 * </p>
	 * @param reference , the PartType, it is not null
	 * @param target, a set of PartType, it must not be null
	 * @throws ConflictingRuleException
	 * @see PartType 
	 * @see Configuration 
	 */
    void addRequirements(PartType reference, Set<PartType> target) throws ConflictingRuleException;
    /**
	 * <p>
	 * 		For a PartType reference, add a PartType target  its requirements in the configuration
	 * 		
	 *	<ul>
	 * 		<li>
	 * 			it removes the target to requirements in the configuration if the PartType reference exists in the configuration
	 *  	</li>
		 * </ul>
		 * </p>
	 * @param reference , the PartType, it is not null
	 * @param target, a PartType, it must not be null
	 * @throws ConflictingRuleException
	 * @see PartType 
	 * @see Configuration 
	 */
    void removeRequirement(PartType reference, PartType target);

}
