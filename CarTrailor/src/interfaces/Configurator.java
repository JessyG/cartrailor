package interfaces;
import java.util.Set;
/**
 * <b>Interface Configurator representing the data access point.</b>
 * <p>
 * A Configurator is represented by the following informations :
 * <ul>
 * <li>A Configuration.</li>
 * <li>A set of PartTypes.</li>
 * <li>a Set of Categories.</li>
 * </ul>
 * </p>
 * 
 * @see Configuration
 * @see PartType
 * @see Category
 * @see CompatibilityChecker
 * 
 * @author Jessy Grall
 * @author Kassim Kaba
 */
public interface Configurator {

	/**
	 * Returns the set of categories.
	 */
    Set<Category> getCategories();

    /**
	 * Returns a set of parts belonging to a category
	 */
    Set<PartType> getVariants(Category category);

    /**
	 * Returns a copy of the class.
	 */
    Configuration getConfiguration();

    /**
	 * Returns the set of compatibility rules
	 * 
	 * @see CompatibilityChecker
	 */
    CompatibilityManager getCompatibilityChecker();

}
