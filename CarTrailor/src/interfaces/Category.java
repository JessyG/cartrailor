package interfaces;
/**
 * @author Grall Jessy
 * @author Kaba Kassim
 * <p>
 * data type to organize PartTypes in categories
 * </p>
 * 
 * @see PartType
 */
public interface Category {

	/**
	 * Returns the name of the Category
	 */
    String getName();
}
