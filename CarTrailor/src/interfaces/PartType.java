package interfaces;
/**
 * <b>Interface for a data structure representing a PartType object</b>
 * <p>
 * A PartType is represented by the following information :
 * <ul>
 * <li>A name</li>
 * <li>A Category</li>
 * </ul>
 * </p>
 * 
 * @see Category
 * @author jessy
 * @author Kassim Kaba
 */
public interface PartType {

	/**
	* Returns the name of the PartType
	*/
    public String getName();

    /**
	 * Return the Category of the PartType
	 * 
	 * @see Category
	 */
    public Category getCategory();
    
    /**
	 * Checks if an Object has the same attributs as the class
	 * 
	 * @param obj
	 * 		Object to test
	 * 
	 * @return	true
	 * 		if the object is the same instance as the class and their name is identical
	 * 			false
	 * 		otherwise
	 */
    public boolean equals(Object obj);
}