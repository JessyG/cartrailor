import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import interfaces.CompatibilityManager;
import interfaces.PartType;

public class CompatibilityManagerImp implements CompatibilityManager {
	private final Map<PartType, Set<PartType>> incompatibilities; 
	/**
	 * 
	 */
	private final Map<PartType, Set<PartType>> requirements;
	
	/**
	 * 
	 */
	public CompatibilityManagerImp(Map<PartType, Set<PartType>> i, Map<PartType, Set<PartType>> r) {
		incompatibilities = i;
		requirements = r;
	}
	
	@Override
	public Optional<Set<PartType>> getIncompatibilities(PartType reference) {
		if(incompatibilities.containsKey(reference))
			return Optional.of(incompatibilities.get(reference));
		return Optional.empty();
	}

	@Override
	public Optional<Set<PartType>> getRequirements(PartType reference) {
		if(requirements.containsKey(reference))
			return Optional.of(requirements.get(reference));
		return Optional.empty();
	}

	@Override
	public void addIncompatibilities(PartType reference, Set<PartType> target) throws rules.ConflictingRuleException {
		Objects.requireNonNull(reference);
		Objects.requireNonNull(target);
		Optional<Set<PartType>> opreq = getRequirements(reference);
		if(opreq.isPresent()) {
			Set<PartType> req = opreq.get(); 
			for(PartType p : target)
				if(req.contains(p)) {
					throw new rules.ConflictingRuleException();
				}
		}
		if(!getIncompatibilities(reference).isPresent())
			incompatibilities.put(reference, target);
		else // reference already exists
			getIncompatibilities(reference).get().addAll(target);
	}

	@Override
	public void removeIncompatibility(PartType reference, PartType target) {
		Objects.requireNonNull(reference);
		Objects.requireNonNull(target);
		Optional<Set<PartType>> p = getIncompatibilities(reference);
		if(p.isPresent())
			p.get().remove(target);
	}

	@Override
	public void addRequirements(PartType reference, Set<PartType> target) throws rules.ConflictingRuleException {
		Objects.requireNonNull(reference);
		Objects.requireNonNull(target);
		Optional<Set<PartType>> opIncomp = getIncompatibilities(reference);
		if(opIncomp.isPresent()) {
			Set<PartType> incomp = opIncomp.get();
			for(PartType p : target)
				if(incomp.contains(p))
					throw new rules.ConflictingRuleException();
		}
		if(!getRequirements(reference).isPresent())
			requirements.put(reference, target);
		else // la reference existe déjà
			getRequirements(reference).get().addAll(target);
	}

	@Override
	public void removeRequirement(PartType reference, PartType target) {
		Objects.requireNonNull(reference);
		Objects.requireNonNull(target);
		Optional<Set<PartType>> p = getRequirements(reference);
		if(p.isPresent())
			p.get().remove(target);
	}

}
