import interfaces.Category;
import interfaces.PartType;

/**
 * Implementation class of PartType
 * 
 * @see PartType
 * 
 * @author Jessy Grall
 * @author Kassim Kaba
 */
public class PartTypeImp implements PartType {
	/**
	 * Name of PartType
	 */
	private String name;
	
	/**
	 * Category of PartType
	 * 
	 * @see Category
	 */
	private Category category;
	
	/**
	 * Class constructor
	 * 
	 * <p>
	 * When building PartTypeImp, we initialize its name and its associated category
	 * </p>
	 * 
	 * @see Category
	 */
	public PartTypeImp(String name, Category cat) {
		this.name = name;
		category = cat;
	}
	
	/**
	 * Returns the name of the PartType
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * Return the Category of the PartType
	 * 
	 * @see Category
	 */
	@Override
	public Category getCategory() {
		return category;
	}
	
	/**
	 * Checks if an Object has the same attributs as the class
	 * 
	 * @param obj
	 * 		Object to test
	 * 
	 * @return	true
	 * 		if the object is the same instance as the class and their name is identical
	 * 			false
	 * 		otherwise
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(!(obj instanceof PartTypeImp)) return false;
		PartTypeImp p = (PartTypeImp) obj;
		return p.name != null && this.name == p.name && this.getCategory().equals(p.getCategory());
	}
	
	/**
	 * Redefining the hashCode operation
	 */
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
}
